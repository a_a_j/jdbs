package ru.zharikov.jdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.zharikov.jdbc.db.Utility;
import ru.zharikov.jdbc.db.mapper.GroupMapper;
import ru.zharikov.jdbc.db.model.Group;
import ru.zharikov.jdbc.db.model.User;
import ru.zharikov.jdbc.db.service.DbService;

import java.util.Map;

@Controller
public class GroupController {
    private DbService dbService;

    @Autowired
    public void setDbService(DbService dbService) {
        this.dbService = dbService;
    }

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public String getAllGroups(Model model) {
        model.addAttribute("all_groups", dbService.getAllGroups());
        return Utility.GROUPS;
    }

    @RequestMapping(value = "/group/{id}", method = RequestMethod.GET)
    public String findUsersByGroup(Model model, @PathVariable int id) {
        User currentUser = Utility.getCurrentUser(dbService);
        boolean isAdmin = Utility.isUserAdmin();
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("group", dbService.getGroupById(id));
        model.addAttribute("students", dbService.getStudentsByGroup(id));
        model.addAttribute("professors", dbService.getProfessorsByGroup(id));
        model.addAttribute("positions", dbService.getPositions());
        return Utility.USERS;
    }

    @RequestMapping(value = "/add_group", method = RequestMethod.POST)
    public String addGroup(@RequestParam("group_name") String name) {
        if (Utility.isUserAdmin()) {
            Group group = new Group(0, name);
            dbService.createStudyGroup(group);
        }
        return Utility.REDIRECT_TO_GROUPS;
    }

    @RequestMapping(value = "/group/{id}/delete", method = RequestMethod.GET)
    public String deleteGroup(@PathVariable int id) {
        if (Utility.isUserAdmin()) {
            dbService.deleteStudyGroup(id);
        }
        return Utility.REDIRECT_TO_GROUPS;
    }

    @RequestMapping(value = "/group/{id}/edit", method = RequestMethod.GET)
    public String editGroup(Model model, @PathVariable int id) {
        if (Utility.isUserAdmin()) {
            Group group = dbService.getGroupById(id);
            model.addAttribute("group", group);
            return Utility.GROUP;
        } else {
            return Utility.REDIRECT_TO_GROUPS;
        }
    }

    @RequestMapping(value = "/group/{id}/edit", method = RequestMethod.POST)
    public String editGroup(@RequestParam Map<String,String> allParams) {
        if (Utility.isUserAdmin()) {
            Group group = new GroupMapper().mapModel(allParams);
            dbService.updateStudyGroup(group);
        }
        return Utility.REDIRECT_TO_GROUPS;
    }

}
