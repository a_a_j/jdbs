package ru.zharikov.jdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.zharikov.jdbc.db.Utility;
import ru.zharikov.jdbc.db.mapper.UserMapper;
import ru.zharikov.jdbc.db.model.Group;
import ru.zharikov.jdbc.db.model.User;
import ru.zharikov.jdbc.db.service.DbService;

import java.util.Map;

@Controller
public class UserController {
    private DbService dbService;

    @Autowired
    public void setDbService(DbService dbService) {
        this.dbService = dbService;
    }

    @RequestMapping(value = "/add_user", method = RequestMethod.POST)
    public String addUser(@RequestParam Map<String,String> allParams) {
        if (Utility.isUserAdmin()) {
            User user = (new UserMapper()).mapModel(allParams);
            dbService.saveUser(user, Integer.parseInt(allParams.get("group_id")));
            return Utility.REDIRECT_TO_GROUP + allParams.get("group_id");
        } else {
            return Utility.REDIRECT_TO_GROUPS;
        }
    }

    @RequestMapping(value = "/users/{id}/delete", method = RequestMethod.GET)
    public String deleteUser(@PathVariable int id) {
        if (Utility.isUserAdmin()) {
            Group group = dbService.getGroupByUserId(id);
            dbService.deleteUser(id);
            return Utility.REDIRECT_TO_GROUP + group.getId();
        } else {
            return Utility.REDIRECT_TO_GROUPS;
        }
    }

    @RequestMapping(value = "/users/{id}/edit", method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable int id) {
        User currentUser = Utility.getCurrentUser(dbService);
        if (Utility.isUserAdmin() || currentUser.getId().equals(id)) {
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("user", dbService.getUserById(id));
            model.addAttribute("group", dbService.getGroupByUserId(id));
            model.addAttribute("groups", dbService.getAllGroups());
            return Utility.USER;
        } else {
            return Utility.REDIRECT_TO_GROUPS;
        }
    }

    @RequestMapping(value = "/users/{id}/edit", method = RequestMethod.POST)
    public String editUser(@RequestParam Map<String,String> allParams) {
        if (Utility.isUserAdmin()) {
            User user = new UserMapper().mapModel(allParams);
            int groupId = Integer.parseInt(allParams.get("group_id"));
            dbService.saveUser(user, groupId);
            return Utility.REDIRECT_TO_GROUP + groupId;
        } else {
            return Utility.REDIRECT_TO_GROUPS;
        }
    }

}
