package ru.zharikov.jdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.zharikov.jdbc.db.service.DbService;

@Controller
public class LoginController {
    private DbService dbService;

    @Autowired
    public void setDbService(DbService dbService) {
        this.dbService = dbService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginForm(@RequestParam(value = "error", required = false) String error, Model model) {
        model.addAttribute("loginError", error);
        return "login";
    }

    /*@RequestMapping(value = "/admin/main", method = RequestMethod.GET)
    public String showUserPage(Model model) {
        model.addAttribute("all_groups", dbService.getAllStudyGroups());
        return "groups";
    }*/
}
