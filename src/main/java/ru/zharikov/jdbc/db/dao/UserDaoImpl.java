package ru.zharikov.jdbc.db.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.zharikov.jdbc.db.Utility;
import ru.zharikov.jdbc.db.dao.interfaces.UserDao;
import ru.zharikov.jdbc.db.mapper.GroupMapper;
import ru.zharikov.jdbc.db.mapper.PositionMapper;
import ru.zharikov.jdbc.db.mapper.UserMapper;
import ru.zharikov.jdbc.db.model.Group;
import ru.zharikov.jdbc.db.model.Position;
import ru.zharikov.jdbc.db.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDaoImpl implements UserDao {
    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class.getName());
    private JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM users ORDER BY id", new UserMapper());
    }

    @Override
    public User findById(int id) {
        User user = null;
        try {
            user = jdbcTemplate.queryForObject(
                   "SELECT * FROM users WHERE id = ?",
                   new Object[]{id},
                   new UserMapper()
            );
        } catch (DataAccessException dae) {
            LOGGER.log(Level.SEVERE, Utility.SQL_EXCEPTION, dae);
        }
        return user;
    }

    @Override
    public User findByUsername(String userName) {
        User user = null;
        try {
            user = jdbcTemplate.queryForObject(
                   "SELECT * FROM users WHERE username = ?",
                   new Object[]{userName},
                   new UserMapper()
            );
        } catch (DataAccessException dae) {
            LOGGER.log(Level.SEVERE, Utility.SQL_EXCEPTION, dae);
        }
        return user;
    }

    @Override
    public List<User> findStudentsByGroup(int id) {
        return jdbcTemplate.query("SELECT u.* FROM users u, group_users g " +
                "WHERE g.user_id = u.id AND u.position_id = 1 AND g.group_id = ? " +
                "ORDER BY u.id", new Object[]{id}, new UserMapper());
    }
    @Override
    public List<User> findProfessorsByGroup(int id) {
        return jdbcTemplate.query("SELECT u.* FROM users u, group_users g " +
                "WHERE g.user_id = u.id AND u.position_id = 2 AND g.group_id = ? " +
                "ORDER BY u.id", new Object[]{id}, new UserMapper());
    }

    @Override
    public User save(User user, Integer groupId) {
        if (user.getId() == 0) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                    .withTableName("users")
                    .usingGeneratedKeyColumns("id");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("username", user.getUsername());
            parameters.put("password", user.getPassword());
            parameters.put("enabled", user.getEnabled());
            parameters.put("role_id", user.getRoleId());
            parameters.put("position_id", user.getPositionId());
            parameters.put("first_name", user.getFirstName());
            parameters.put("last_name", user.getLastName());
            parameters.put("birth_date", user.getBirthDate());
            Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
            user.setId(id.intValue());
            simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                    .withTableName("group_users")
                    .usingGeneratedKeyColumns("id");
            parameters.clear();
            parameters.put("group_id", groupId);
            parameters.put("user_id", id.intValue());
            simpleJdbcInsert.executeAndReturnKey(parameters);

        } else {
            jdbcTemplate.update("UPDATE users SET username = ?, password = COALESCE(?,password), " +
                            "enabled = COALESCE(?, enabled), position_id = COALESCE(?, position_id), " +
                            "first_name = ?, last_name = ?, birth_date = ? WHERE id = ?",
                    user.getUsername(), user.getPassword(), user.getEnabled(), user.getPositionId(),
                    user.getFirstName(), user.getLastName(), user.getBirthDate(), user.getId());
        }
        return user;
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM users WHERE id = ?", id);
    }

    @Override
    public List<Position> getPositions() {
        return jdbcTemplate.query("SELECT * FROM positions WHERE id IN (1,2)",
                new PositionMapper());
    }

    @Override
    public Group getGroupByUserId(int id) {
        Group group = null;
        try {
            group = jdbcTemplate.queryForObject("SELECT g.* FROM groups g, users u, group_users gu " +
                            "WHERE gu.user_id = u.id AND gu.group_id = g.id AND u.id = ?",
                    new Object[]{id}, new GroupMapper());
        } catch (DataAccessException dae) {
            LOGGER.log(Level.SEVERE, Utility.SQL_EXCEPTION, dae);
        }
        return group;
    }
}
