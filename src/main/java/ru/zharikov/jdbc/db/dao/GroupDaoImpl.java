package ru.zharikov.jdbc.db.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import ru.zharikov.jdbc.db.Utility;
import ru.zharikov.jdbc.db.dao.interfaces.GroupDao;
import ru.zharikov.jdbc.db.mapper.GroupMapper;
import ru.zharikov.jdbc.db.model.Group;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GroupDaoImpl implements GroupDao {
    private static final Logger LOGGER = Logger.getLogger(GroupDaoImpl.class.getName());
    private JdbcTemplate jdbcTemplate;

    public GroupDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Group getGroupById(int id) {
        Group group = null;
        try {
            group = jdbcTemplate.queryForObject(
                    "SELECT * FROM groups WHERE id = ?",
                    new Object[]{id},
                    new GroupMapper()
            );
        } catch (DataAccessException dae) {
            LOGGER.log(Level.SEVERE, Utility.SQL_EXCEPTION, dae);
        }
        return group;
    }

    @Override
    public Group saveGroup(Group group) {
        if (group.getId() == 0) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                    .withTableName("groups")
                    .usingGeneratedKeyColumns("id");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("group_name", group.getName());
            Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
            group.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE groups SET group_name = ? WHERE id = ?",
                    group.getName(), group.getId());
        }
        return group;
    }

    @Override
    public void deleteGroup(int id) {
        jdbcTemplate.update("DELETE FROM groups WHERE id = ?", id);
    }

    @Override
    public List<Group> getAllGroups() {
        return jdbcTemplate.query("SELECT * FROM groups ORDER BY id", new GroupMapper());
    }
}
