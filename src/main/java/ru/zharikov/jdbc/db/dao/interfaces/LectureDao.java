package ru.zharikov.jdbc.db.dao.interfaces;

import ru.zharikov.jdbc.db.model.Lecture;

import java.util.List;

public interface LectureDao {
    Lecture getLectureById(int id);
    void updateLecture(Lecture lecture);
    Lecture insertLecture(Lecture lecture);
    void deleteLecture(int id);
    List<Lecture> getAllLectures();
    List<Lecture> getLecturesByStudyGroup(int studyGroupId);
}
