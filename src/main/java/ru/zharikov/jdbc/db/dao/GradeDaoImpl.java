package ru.zharikov.jdbc.db.dao;

import ru.zharikov.jdbc.db.Utility;
import ru.zharikov.jdbc.db.connection.ConnectionPool;
import ru.zharikov.jdbc.db.dao.interfaces.GradeDao;
import ru.zharikov.jdbc.db.mapper.GradeMapper;
import ru.zharikov.jdbc.db.model.Grade;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GradeDaoImpl implements GradeDao {
    private ConnectionPool connectionPool;
    private Logger log = Logger.getLogger(GradeDaoImpl.class.getName());

    public GradeDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public Grade getGradeById(int id) {
        Grade grade = new Grade();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * FROM grades WHERE id = ?")) {
                    preparedStatement.setInt(1, id);
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        if (resultSet.next()) {
                            grade = GradeMapper.ResultSetToModel(resultSet);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
        return grade;
    }

    @Override
    public void updateGrade(Grade grade) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "UPDATE grades SET lecture_id = ?, student_id = ?, grade_value = ? WHERE id = ?")) {
                    GradeMapper.mapStatementToUpdate(preparedStatement, grade);
                    preparedStatement.executeUpdate();
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
    }

    @Override
    public Grade insertGrade(Grade grade) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "INSERT INTO grades (lecture_id, student_id, grade_value) VALUES (?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS)) {
                    GradeMapper.mapStatementToInsert(preparedStatement, grade);
                    preparedStatement.executeUpdate();
                    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                        if (resultSet.next()) {
                            grade.setId(resultSet.getInt(1));
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
        return grade;
    }

    @Override
    public void deleteGrade(int id) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "DELETE FROM grades WHERE id = ?")) {
                    preparedStatement.setInt(1, id);
                    preparedStatement.executeUpdate();
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
    }

    @Override
    public List<Grade> getAllGrades() {
        List<Grade> grades = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * FROM grades ORDER BY ID")) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            Grade grade = GradeMapper.ResultSetToModel(resultSet);
                            grades.add(grade);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
        return grades;
    }

    @Override
    public List<Grade> getGradesByLecture(int lectureId) {
        List<Grade> grades = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * FROM grades where lecture_id = ?")) {
                    preparedStatement.setInt(1, lectureId);
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            Grade grade = GradeMapper.ResultSetToModel(resultSet);
                            grades.add(grade);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
        return grades;
    }
}
