package ru.zharikov.jdbc.db.dao.interfaces;

import ru.zharikov.jdbc.db.model.Group;

import java.util.List;

public interface GroupDao {
    Group getGroupById(int id);
    Group saveGroup(Group group);
    void deleteGroup(int id);
    List<Group> getAllGroups();
}
