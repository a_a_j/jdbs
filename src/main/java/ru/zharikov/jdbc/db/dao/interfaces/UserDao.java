package ru.zharikov.jdbc.db.dao.interfaces;

import ru.zharikov.jdbc.db.model.Group;
import ru.zharikov.jdbc.db.model.Position;
import ru.zharikov.jdbc.db.model.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();
    User findById(int id);
    User findByUsername(String userName);
    List<User> findStudentsByGroup(int id);
    List<User> findProfessorsByGroup(int id);
    User save(User user, Integer groupId);
    void delete(int id);
    List<Position> getPositions();
    Group getGroupByUserId(int id);
}
