package ru.zharikov.jdbc.db.dao;

import ru.zharikov.jdbc.db.connection.ConnectionPool;
import ru.zharikov.jdbc.db.dao.interfaces.LectureDao;
import ru.zharikov.jdbc.db.mapper.LectureMapper;
import ru.zharikov.jdbc.db.model.Lecture;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LectureDaoImpl implements LectureDao {
    private ConnectionPool connectionPool;
    private Logger log = Logger.getLogger(LectureDaoImpl.class.getName());

    public LectureDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public Lecture getLectureById(int id) {
        Lecture lecture = new Lecture();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * from lecture where id = ?")) {
                    preparedStatement.setInt(1, id);
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        if (resultSet.next()) {
                            lecture = LectureMapper.ResultSetToModel(resultSet);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lecture;
    }

    @Override
    public void updateLecture(Lecture lecture) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "UPDATE lecture SET event_date = ?, group_id = ?, topic = ?, classroom = ? " +
                                "WHERE id = ?")) {
                    LectureMapper.mapStatementToUpdate(preparedStatement, lecture);
                    preparedStatement.executeUpdate();
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQL Exception", e);
        }
    }

    @Override
    public Lecture insertLecture(Lecture lecture) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "INSERT INTO lecture (event_date, group_id, topic, classroom) " +
                                "VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
                    LectureMapper.mapStatementToInsert(preparedStatement, lecture);
                    preparedStatement.executeUpdate();
                    try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                        if (resultSet.next()) {
                            lecture.setId(resultSet.getInt(1));
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQLException", e);
        }
        return lecture;
    }

    @Override
    public void deleteLecture(int id) {
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "DELETE FROM lecture WHERE id = ?")) {
                    preparedStatement.setInt(1, id);
                    preparedStatement.executeUpdate();
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQLException", e);
        }
    }

    @Override
    public List<Lecture> getAllLectures() {
        List<Lecture> lectures = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * FROM lecture ORDER BY ID")) {
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            Lecture lecture = LectureMapper.ResultSetToModel(resultSet);
                            lectures.add(lecture);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQLException", e);
        }
        return lectures;
    }

    @Override
    public List<Lecture> getLecturesByStudyGroup(int studyGroupId) {
        List<Lecture> lectures = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(
                        "SELECT * FROM lecture where group_id = ? and event_date > current_date")) {
                    preparedStatement.setInt(1, studyGroupId);
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        while(resultSet.next()) {
                            Lecture lecture = LectureMapper.ResultSetToModel(resultSet);
                            lectures.add(lecture);
                        }
                    }
                }
                finally {
                    connectionPool.releaseConnection(connection);
                }
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQLException", e);
        }
        return lectures;
    }
}
