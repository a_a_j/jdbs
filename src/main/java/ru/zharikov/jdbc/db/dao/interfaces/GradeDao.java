package ru.zharikov.jdbc.db.dao.interfaces;

import ru.zharikov.jdbc.db.model.Grade;

import java.util.List;

public interface GradeDao {
    Grade getGradeById(int id);
    void updateGrade(Grade grade);
    Grade insertGrade(Grade grade);
    void deleteGrade(int id);
    List<Grade> getAllGrades();
    List<Grade> getGradesByLecture(int lectureId);
}
