package ru.zharikov.jdbc.db;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import ru.zharikov.jdbc.db.connection.ConnectionPool;
import ru.zharikov.jdbc.db.dao.*;
import ru.zharikov.jdbc.db.dao.interfaces.*;
import ru.zharikov.jdbc.db.service.DbService;

import javax.sql.DataSource;

@Configuration
@ComponentScan("ru.zharikov.jdbc")
public class AppConfiguration {
    private static final int MAX_CONNECTION_COUNT = 5;

    @Bean
    public DbService dbService(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return new DbService(userDao(), lectureDao(), studyGroupDao(), gradeDao());
    }

    @Bean
    public UserDao userDao() {
        return new UserDaoImpl(myJdbcTemplate());
    }

    @Bean
    public LectureDao lectureDao() {
        return new LectureDaoImpl(connectionPool());
    }

    @Bean
    public GroupDao studyGroupDao() {
        return new GroupDaoImpl(myJdbcTemplate());
    }

    @Bean
    public GradeDao gradeDao() {
        return new GradeDaoImpl(connectionPool());
    }

    @Bean
    public ConnectionPool connectionPool() {
        return ConnectionPool.getInstance(MAX_CONNECTION_COUNT);
    }

    @Bean
    public JdbcTemplate myJdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");
        return dataSource;
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/pages/");
        bean.setSuffix(".jsp");
        return bean;
    }

}
