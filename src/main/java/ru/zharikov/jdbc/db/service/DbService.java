package ru.zharikov.jdbc.db.service;

import ru.zharikov.jdbc.db.dao.interfaces.*;
import ru.zharikov.jdbc.db.model.*;

import java.util.List;

public class DbService {
    private UserDao userDao;
    private LectureDao lectureDao;
    private GroupDao groupDao;
    private GradeDao gradeDao;

    public DbService() {
    }

    public DbService(UserDao userDao, LectureDao lectureDao, GroupDao groupDao, GradeDao gradeDao) {
        this.userDao = userDao;
        this.lectureDao = lectureDao;
        this.groupDao = groupDao;
        this.gradeDao = gradeDao;
    }

    public User getUserById(int id) {
        return userDao.findById(id);
    }

    public User getUserByUsername(String userName) {
        return userDao.findByUsername(userName);
    }

    public User saveUser(User user, Integer groupId) {
        return userDao.save(user, groupId);
    }

    public void deleteUser(int id) {
        userDao.delete(id);
    }

    public Group getGroupByUserId(int id) {
        return userDao.getGroupByUserId(id);
    }

    public List<User> getStudentsByGroup(int groupId) {
        return userDao.findStudentsByGroup(groupId);
    }
    public List<User> getProfessorsByGroup(int groupId) {
        return userDao.findProfessorsByGroup(groupId);
    }

    public List<Position> getPositions() {
        return userDao.getPositions();
    }

    public Lecture getLectureById(int id) {
        return lectureDao.getLectureById(id);
    }

    public Lecture createLecture(Lecture lecture) {
        return lectureDao.insertLecture(lecture);
    }

    public void updateLecture(Lecture lecture) {
        lectureDao.updateLecture(lecture);
    }

    public void deleteLecture(int id) {
        lectureDao.deleteLecture(id);
    }

    public List<Lecture> getAllLectures() {
        return lectureDao.getAllLectures();
    }

    public List<Lecture> getLecturesByStudyGroup(int studyGroupId) {
        return lectureDao.getLecturesByStudyGroup(studyGroupId);
    }

    public Group getGroupById(int id) {
        return groupDao.getGroupById(id);
    }

    public Group createStudyGroup(Group group) {
        return groupDao.saveGroup(group);
    }

    public void updateStudyGroup(Group group) {
        groupDao.saveGroup(group);
    }

    public void deleteStudyGroup(int id) {
        groupDao.deleteGroup(id);
    }

    public List<Group> getAllGroups() {
        return groupDao.getAllGroups();
    }

    public Grade getGradeById(int id) {
        return gradeDao.getGradeById(id);
    }

    public Grade createGrade(Grade grade) {
        return gradeDao.insertGrade(grade);
    }

    public void updateGrade(Grade grade) {
        gradeDao.updateGrade(grade);
    }

    public void deleteGrade(int id) {
        gradeDao.deleteGrade(id);
    }

    public List<Grade> getAllGrades() {
        return gradeDao.getAllGrades();
    }

    public List<Grade> getGradesByLecture(int lectureId) {
        return gradeDao.getGradesByLecture(lectureId);
    }

}
