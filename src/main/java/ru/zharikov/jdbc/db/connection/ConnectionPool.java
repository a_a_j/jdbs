package ru.zharikov.jdbc.db.connection;

import ru.zharikov.jdbc.db.Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionPool {
    private static volatile ConnectionPool instance;
    private List<Connection> connections;
    private int limit;
    private Logger log = Logger.getLogger(ConnectionPool.class.getName());

    private ConnectionPool(int limit) {
        this.limit = limit;
        this.connections = new ArrayList<>();
    }

    public static ConnectionPool getInstance(int limit) {
        ConnectionPool connectionPool = instance;
        if (connectionPool == null) {
            synchronized (ConnectionPool.class) {
                connectionPool = instance;
                if (connectionPool == null) {
                    instance = connectionPool = new ConnectionPool(limit);
                }
            }
        }
        return connectionPool;
    }

    public Connection getConnection() {
        Connection connection = null;
        if (instance.connections.size() < limit) {
            try {
                connection = DriverManager.getConnection
                        ("jdbc:postgresql://localhost:5432/test",
                                "postgres", "postgres");
            } catch (SQLException e) {
                log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
            }
            instance.connections.add(connection);
        }
        return connection;
    }

    public void releaseConnection(Connection connection){
        try {
            connection.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE, Utility.SQL_EXCEPTION, e);
        }
        instance.connections.remove(connection);
    }
}
