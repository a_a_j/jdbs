package ru.zharikov.jdbc.db;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.zharikov.jdbc.db.model.User;
import ru.zharikov.jdbc.db.service.DbService;

public final class Utility {
    public static final String SQL_EXCEPTION = "SQL Exception";
    public static final String GROUP = "group";
    public static final String GROUPS = "groups";
    public static final String REDIRECT_TO_GROUP = "redirect:/group/";
    public static final String REDIRECT_TO_GROUPS = "redirect:/groups";
    public static final String USER = "user";
    public static final String USERS = "users";

    private Utility() {
    }

    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        return new java.sql.Date(uDate.getTime());
    }

    public static boolean isUserAdmin() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    public static User getCurrentUser(DbService dbService) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        return dbService.getUserByUsername(currentUserName);
    }
}
