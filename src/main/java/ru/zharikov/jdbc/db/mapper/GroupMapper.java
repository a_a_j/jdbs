package ru.zharikov.jdbc.db.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.zharikov.jdbc.db.model.Group;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class GroupMapper implements RowMapper<Group> {
    @Override
    public Group mapRow(ResultSet resultSet, int i) throws SQLException {
        Group group = new Group();
        group.setId(resultSet.getInt("id"));
        group.setName(resultSet.getString("group_name"));
        return group;
    }

    public Group mapModel(Map<String, String> params) {
        Group group = new Group();
        if (params.get("group_id").length() > 0) {
            group.setId(Integer.parseInt(params.get("group_id")));
        }
        group.setName(params.get("group_name"));
        return group;
    }
}
