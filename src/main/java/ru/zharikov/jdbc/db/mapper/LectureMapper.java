package ru.zharikov.jdbc.db.mapper;

import ru.zharikov.jdbc.db.model.Lecture;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LectureMapper {
    public static Lecture ResultSetToModel(ResultSet resultSet) throws SQLException {
        Lecture lecture = new Lecture();
        lecture.setId(resultSet.getInt(1));
        lecture.setEventDate(resultSet.getDate(2));
        lecture.setGroupId(resultSet.getInt(3));
        lecture.setTopic(resultSet.getString(4));
        lecture.setClassRoom(resultSet.getInt(5));
        return lecture;
    }

    public static void mapStatementToInsert(PreparedStatement preparedStatement, Lecture lecture) throws SQLException {
        preparedStatement.setDate(1, lecture.getEventDate());
        preparedStatement.setInt(2, lecture.getGroupId());
        preparedStatement.setString(3, lecture.getTopic());
        preparedStatement.setInt(4, lecture.getClassRoom());
    }

    public static void mapStatementToUpdate(PreparedStatement preparedStatement, Lecture lecture) throws SQLException {
        mapStatementToInsert(preparedStatement, lecture);
        preparedStatement.setInt(5, lecture.getId());
    }
}
