package ru.zharikov.jdbc.db.mapper;

import ru.zharikov.jdbc.db.model.Grade;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GradeMapper {
    public static Grade ResultSetToModel(ResultSet resultSet) throws SQLException {
        Grade grade = new Grade();
        grade.setId(resultSet.getInt(1));
        grade.setLectureId(resultSet.getInt(2));
        grade.setStudentId(resultSet.getInt(3));
        grade.setGradeValue(resultSet.getInt(4));
        return grade;
    }

    public static void mapStatementToInsert(PreparedStatement preparedStatement, Grade grade) throws SQLException {
        preparedStatement.setInt(1, grade.getLectureId());
        preparedStatement.setInt(2, grade.getStudentId());
        preparedStatement.setInt(3, grade.getGradeValue());
    }

    public static void mapStatementToUpdate(PreparedStatement preparedStatement, Grade grade) throws SQLException {
        mapStatementToInsert(preparedStatement, grade);
        preparedStatement.setInt(4, grade.getId());
    }
}
