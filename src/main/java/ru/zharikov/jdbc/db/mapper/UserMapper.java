package ru.zharikov.jdbc.db.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCrypt;
import ru.zharikov.jdbc.db.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserMapper implements RowMapper<User> {
    private static final Logger LOGGER = Logger.getLogger(UserMapper.class.getName());

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        user.setEnabled(resultSet.getInt("enabled"));
        user.setRoleId(resultSet.getInt("role_id"));
        user.setPositionId(resultSet.getInt("position_id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setBirthDate(resultSet.getDate("birth_date"));
        return user;
    }

    public User mapModel(Map<String, String> params) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate = new Date();
        try {
            birthDate = simpleDateFormat.parse(params.get("birth_date"));
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, "Birth date conversion error!", e);
        }
        User user = new User();
        user.setId(Integer.parseInt(params.get("id")));
        user.setUsername(params.get("username"));
        user.setFirstName(params.get("first_name"));
        user.setLastName(params.get("last_name"));
        user.setBirthDate(birthDate);
        if (params.get("password") != null && !params.get("password").isEmpty()) {
            user.setPassword(BCrypt.hashpw(params.get("password"), BCrypt.gensalt()));
        }
        if (params.get("enabled") != null && !params.get("enabled").isEmpty()) {
            user.setEnabled(Integer.parseInt(params.get("enabled")));
        }
        if (params.get("role_id") != null) {
            user.setRoleId(Integer.parseInt(params.get("role_id")));
        }
        if (params.get("position_id") != null && !params.get("position_id").isEmpty()) {
            user.setPositionId(Integer.parseInt(params.get("position_id")));
        }
        return user;
    }
}
