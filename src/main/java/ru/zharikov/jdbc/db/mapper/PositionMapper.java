package ru.zharikov.jdbc.db.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.zharikov.jdbc.db.model.Position;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PositionMapper implements RowMapper<Position> {
    @Override
    public Position mapRow(ResultSet resultSet, int i) throws SQLException {
        Position position = new Position();
        position.setId(resultSet.getInt("id"));
        position.setName(resultSet.getString("name"));
        return position;
    }
}
