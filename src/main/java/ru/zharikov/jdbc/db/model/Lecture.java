package ru.zharikov.jdbc.db.model;

import java.sql.Date;

public class Lecture {
    private Integer id;
    private Date eventDate;
    private Integer groupId;
    private String topic;
    private int classRoom;

    public Lecture() {
    }

    public Lecture(Integer id, Date eventDate, Integer idGroup, String topic, int classRoom) {
        this.id = id;
        this.eventDate = eventDate;
        this.groupId = idGroup;
        this.topic = topic;
        this.classRoom = classRoom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(int classRoom) {
        this.classRoom = classRoom;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "id=" + id +
                ", eventDate=" + eventDate +
                ", groupId=" + groupId +
                ", topic='" + topic + '\'' +
                ", classRoom=" + classRoom +
                '}';
    }
}
