package ru.zharikov.jdbc.db.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer enabled;
    private Integer roleId;
    private Integer positionId;
    private String firstName;
    private String lastName;
    private Date birthDate;
}
