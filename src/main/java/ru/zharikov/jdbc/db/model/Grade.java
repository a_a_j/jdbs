package ru.zharikov.jdbc.db.model;

public class Grade {
    private Integer id;
    private Integer lectureId;
    private Integer studentId;
    private int gradeValue;

    public Grade() {
    }

    public Grade(Integer id, Integer lectureId, Integer studentId, int gradeValue) {
        this.id = id;
        this.lectureId = lectureId;
        this.studentId = studentId;
        this.gradeValue = gradeValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public int getGradeValue() {
        return gradeValue;
    }

    public void setGradeValue(int gradeValue) {
        this.gradeValue = gradeValue;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", lectureId=" + lectureId +
                ", studentId=" + studentId +
                ", gradeValue=" + gradeValue +
                '}';
    }
}
