<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
    function deleteUser(id) {
        var toDelete = confirm("Are you sure you want to delete this user?");
        if (toDelete) {
            window.location.href = "/users/"+id+"/delete";
        }
    }
</script>
<html>
    <head>
        <title>${group.name} Group</title>
    </head>
    <body>
        <h1>${group.name} Group</h1>
        <h3>Students</h3>
        <c:if test="${students.size()==0}"><h5>Not applied yet</h5></c:if>
        <table>
            <c:forEach items="${students}" var="student">
                <tr>
                    <td>
                        ${student.firstName} ${student.lastName}
                    </td>
                    <td>
                        <c:if test="${currentUser eq student || isAdmin}">
                            <a href="/users/${student.id}/edit">[edit]</a>
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${isAdmin}">
                            <a href="javascript:deleteUser(${student.id})">[x]</a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <h3>Professors</h3>
        <c:if test="${professors.size()==0}"><h5>Not assigned yet</h5></c:if>
        <table>
            <c:forEach items="${professors}" var="professor">
                <tr>
                    <td>
                            ${professor.firstName} ${professor.lastName}
                    </td>
                    <td>
                        <c:if test="${currentUser eq professor || isAdmin}">
                            <a href="/users/${professor.id}/edit">[edit]</a>
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${isAdmin}">
                            <a href="javascript:deleteUser(${professor.id})">[x]</a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <form action="/add_user" method="post">
                <input type="hidden" name="id" value="0">
                <input type="hidden" name="group_id" value="${group.id}">
                <input type="hidden" name="role_id" value="2">
                <input type="hidden" name="enabled" value="1">
                <table>
                    <tr>
                        <th colspan="2" align="left">Add new person:</th>
                    </tr>
                    <tr>
                        <td>Position</td>
                        <td>
                            <select name="position_id">
                                <c:forEach items="${positions}" var="pos">
                                    <option value ="${pos.id}">
                                        <c:out value="${pos.name}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>First name</td>
                        <td><input type="text" name="first_name"></td>
                    </tr>
                    <tr>
                        <td>Last name</td>
                        <td><input type="text" name="last_name"></td>
                    </tr>
                    <tr>
                        <td>Birth date</td>
                        <td><input type="date" name="birth_date" value="1985-01-01"
                                   min="1900-01-01" max="2019-01-01"></td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" name="username"></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="Add"></td>
                    </tr>
                </table>
            </form>
        </sec:authorize>
        <p><a href="/groups">Back to groups</a></p>
    </body>
</html>
