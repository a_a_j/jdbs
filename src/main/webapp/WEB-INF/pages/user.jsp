<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${user.firstName} ${user.lastName}</title>
</head>
<body>
    <h1>${user.firstName} ${user.lastName}</h1>
    <form action="/users/${user.id}/edit" method="post">
        <input type="hidden" name="id" value="${user.id}">
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" name="first_name" value="${user.firstName}"></td>
            </tr>
            <tr>
                <td>Last name</td>
                <td><input type="text" name="last_name" value="${user.lastName}"></td>
            </tr>
            <tr>
                <td>Date of birth</td>
                <td><input type="date" name="birth_date" value="${user.birthDate}"
                           min="1900-01-01" max="2019-01-01"></td>
            </tr>
            <tr>
                <td>Group</td>
                <td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <select name="group_id">
                            <c:forEach items="${groups}" var="g">
                                <option value ="${g.id}" ${group.id == g.id ? 'selected="selected"' : ''}>
                                    <c:out value="${g.name}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <input type="hidden" name="group_id" value="${group.id}">
                        ${group.name}
                    </sec:authorize>
                </td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username" value="${user.username}"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save"></td>
            </tr>
        </table>
    </form>
    <p><a href="/group/${group.id}">Back to group</a></p>
</body>
</html>
