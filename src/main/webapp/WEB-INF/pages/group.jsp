<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${group.name}</title>
    </head>
    <body>
        <h1>${group.name}</h1>
        <h3>Edit page</h3>
        <form action="/group/${group.id}/edit" method="post">
            <input type="hidden" name="group_id" value="${group.id}">
            <table>
                <tr>
                    <td>Name</td>
                    <td><input type="text" name="group_name" value="${group.name}"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form>
        <p><a href="/groups/">Back to groups</a></p>
    </body>
</html>
