<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <h1>Login</h1>
        <div>Пожалуйста, авторизуйтесь</div>
        <form action="/j_username_security_check" method="post">
            <table>
                <tr>
                    <td><input type="text" name="j_username" placeholder="Input your login" required></td>
                </tr>
                <tr>
                    <td><input type="password" name="j_password" placeholder="Input your password" required></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Login"></td>
                </tr>
            </table>
            <c:if test="${not empty loginError}">
                <H3 style="color:#CC0000">
                    Неверное имя пользователя, или пароль
                </H3>
            </c:if>
        </form>
    </body>
</html>
