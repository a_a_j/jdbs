<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
    function deleteGroup(id) {
        var toDelete = confirm("Вы уверены, что хотите удалить группу?");
        if (toDelete) {
            window.location.href = "/group/"+id+"/delete";
        }
    }
</script>
<html>
<head>
    <title>Groups</title>
</head>
<body>
    <h1>Groups</h1>
    <table>
        <c:forEach items="${all_groups}" var="group">
            <tr>
                <td><a href="/group/${group.id}">${group.name}</a></td>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <td><a href="/group/${group.id}/edit">[edit]</a></td>
                    <td><a href="javascript:deleteGroup(${group.id})">[x]</a></td>
                </sec:authorize>
            </tr>
        </c:forEach>
    </table>
    <br>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <form action="/add_group" method="post">
            <input type="hidden" name="group_id" value="0">
            <table>
                <tr><th colspan="2" align="left">Add new group:</th></tr>
                <tr>
                    <td>Group name</td>
                    <td><input type="text" name="group_name"/></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Add"></td>
                </tr>
            </table>
        </form>
    </sec:authorize>
    <p><a href="/">Back to homepage</a></p>
    <p><a href="/static/j_spring_security_logout">Logout</a></p>
</body>
</html>
